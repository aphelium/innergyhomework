﻿using InnergyHomework.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InnergyHomework.Extensions
{
    public static class CollectionExtensions
    {
        public static bool HasWarehouse(this IEnumerable<WarehouseMaterialOutput> warehouseMaterialOutputs, string warehouseName)
        {
            return warehouseMaterialOutputs.Select(_ => _.WarehouseName).Contains(warehouseName);
        }

        public static void AppendMaterialToWarehouse(this IEnumerable<WarehouseMaterialOutput> warehouseMaterialOutputs, string warehouseName, string materialId, int materialQuantity)
        {
            foreach (var warehouseMaterialOutput in warehouseMaterialOutputs)
            {
                if (warehouseMaterialOutput.WarehouseName.Equals(warehouseName))
                {
                    warehouseMaterialOutput.AppendMaterial(materialId);
                    warehouseMaterialOutput.AppendQuantityToMaterial(materialId, materialQuantity);
                }
            }
        }

        public static void AppendMaterial(this WarehouseMaterialOutput warehouseMaterialOutput, string materialId)
        {
            if (!warehouseMaterialOutput.MaterialQuantityOutputs
                .Where(_ => _.MaterialId.Equals(materialId))
                .Any())
            {
                var materialQuantityOutputs = warehouseMaterialOutput.MaterialQuantityOutputs.ToList();

                materialQuantityOutputs.Add(new MaterialQuantityOutput()
                {
                    MaterialId = materialId,
                    MaterialQuantity = 0
                });

                warehouseMaterialOutput.MaterialQuantityOutputs = materialQuantityOutputs;
            }
        }

        public static void AppendQuantityToMaterial(this WarehouseMaterialOutput warehouseMaterialOutput, string materialId, int materialQuantity)
        {
            var materialQuantityOutputsTemp = warehouseMaterialOutput.MaterialQuantityOutputs.ToArray();

            for (int i = 0; i < materialQuantityOutputsTemp.Length; i++)
                if (materialQuantityOutputsTemp[i].MaterialId.Equals(materialId))
                    materialQuantityOutputsTemp[i].MaterialQuantity += materialQuantity;

            warehouseMaterialOutput.MaterialQuantityOutputs = materialQuantityOutputsTemp.ToList();
        }
    }
}
