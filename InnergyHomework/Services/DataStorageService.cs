﻿namespace InnergyHomework.Services
{
    public class DataStorageService
    {
        private readonly IDataStorageService _dataStorageService;

        public DataStorageService(IDataStorageService dataStorageService)
        {
            _dataStorageService = dataStorageService;
        }
        public void StoreData(string data)
        {
            _dataStorageService.StoreData(data);
        }
    }
}
