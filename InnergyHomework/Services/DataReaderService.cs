﻿using System.Collections.Generic;

namespace InnergyHomework.Services
{
    public class DataReaderService
    {
        private readonly IDataReaderService _dataReaderService;

        public DataReaderService(IDataReaderService dataReaderService)
        {
            _dataReaderService = dataReaderService;
        }

        public IEnumerable<string> ReadData()
        {
            return _dataReaderService.ReadData();
        }
    }
}
