﻿using InnergyHomework.Formatters;
using InnergyHomework.Models;
using InnergyHomework.Parsers;
using System.Collections.Generic;

namespace InnergyHomework.Services
{
    public class DataFormatService
    {
        private readonly IDataFormatter _dataFormatter;

        public DataFormatService(IDataFormatter dataFormatter)
        {
            _dataFormatter = dataFormatter;
        }

        public string Parse(IEnumerable<MaterialWarehouselInput> inputData)
        {
            if (inputData == null)
                return string.Empty;

            var warehouseMaterialParser = new WarehouseMaterialParser();
            var inputDataParsed = warehouseMaterialParser.Parse(inputData);

            return _dataFormatter.Format(inputDataParsed);
        }
    }
}
