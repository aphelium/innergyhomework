﻿using System.Collections.Generic;

namespace InnergyHomework.Services
{
    public interface IDataReaderService
    {
        IEnumerable<string> ReadData();
    }
}