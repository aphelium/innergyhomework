﻿using System.IO;

namespace InnergyHomework.Services
{
    public class FileDataStorage : IDataStorageService
    {
        private readonly string _outputFileName;

        public FileDataStorage(string outputFileName)
        {
            _outputFileName = outputFileName;
        }

        public void StoreData(string data)
        {
            try
            {
                using StreamWriter streamWriter = new StreamWriter(_outputFileName);
                streamWriter.Write(data);
            }
            catch (System.Exception)
            {
                //
            }
        }
    }
}
