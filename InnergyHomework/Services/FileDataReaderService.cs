﻿using System.Collections.Generic;
using System.IO;

namespace InnergyHomework.Services
{
    public class FileDataReader : IDataReaderService
    {
        private readonly string _inputFileName;

        public FileDataReader(string inputFileName)
        {
            _inputFileName = inputFileName;
        }

        public IEnumerable<string> ReadData()
        {
            var lines = new List<string>();

            try
            {
                using var streamReader = new StreamReader(_inputFileName);
                while (!streamReader.EndOfStream)
                {
                    var line = streamReader.ReadLine();
                    if (!string.IsNullOrWhiteSpace(line) && !line.StartsWith('#'))
                        lines.Add(line);
                }
            }
            catch (IOException)
            {
                //
            }

            return lines;
        }
    }
}
