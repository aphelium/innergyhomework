﻿namespace InnergyHomework.Services
{
    public interface IDataStorageService
    {
        void StoreData(string data);
    }
}