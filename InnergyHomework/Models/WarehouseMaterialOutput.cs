﻿using System.Collections.Generic;
using System.Linq;

namespace InnergyHomework.Models
{
    public class WarehouseMaterialOutput
    {
        public string WarehouseName { get; set; }
        public int WarehouseTotal
        {
            get
            {
                return MaterialQuantityOutputs.Sum(_ => _.MaterialQuantity);
            }
        }

        public IEnumerable<MaterialQuantityOutput> MaterialQuantityOutputs { get; set; } = new List<MaterialQuantityOutput>();
    }
}
