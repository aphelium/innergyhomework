﻿using System.Collections.Generic;

namespace InnergyHomework.Models
{
    public struct MaterialWarehouselInput
    {
        public string MaterialName { get; set; }
        public string MaterialId { get; set; }
        public IEnumerable<MaterialWarehouseQuantityInput> MaterialWarehouseQuantityInputs { get; set; }
    }
}
