﻿namespace InnergyHomework.Models
{
    public struct MaterialQuantityOutput
    {
        public string MaterialId { get; set; }
        public int MaterialQuantity { get; set; }
    }
}
