﻿namespace InnergyHomework.Models
{
    public struct MaterialWarehouseQuantityInput
    {
        public string Warehouse { get; set; }
        public int Quantity { get; set; }
    }
}
