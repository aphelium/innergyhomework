﻿using InnergyHomework.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnergyHomework.Formatters
{
    public class HomeworkDataFormatter : IDataFormatter
    {
        public string Format(IEnumerable<WarehouseMaterialOutput> warehouseMaterialOutputs)
        {
            var result = new StringBuilder();

            foreach (var warehouseMaterialOutput in warehouseMaterialOutputs.ToList().OrderByDescending(_ => _.WarehouseTotal).ThenByDescending(_ => _.WarehouseName))
            {
                result.AppendLine($"{warehouseMaterialOutput.WarehouseName} (total: {warehouseMaterialOutput.WarehouseTotal})");

                foreach (var materialQuantityOutput in warehouseMaterialOutput.MaterialQuantityOutputs.ToList().OrderBy(_ => _.MaterialId))
                {
                    result.AppendLine($"{materialQuantityOutput.MaterialId}: {materialQuantityOutput.MaterialQuantity}");
                }

                result.AppendLine();
            }

            return result.ToString();
        }
    }
}
