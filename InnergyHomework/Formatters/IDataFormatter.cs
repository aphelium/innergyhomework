﻿using InnergyHomework.Models;
using System.Collections.Generic;

namespace InnergyHomework.Formatters
{
    public interface IDataFormatter
    {
        string Format(IEnumerable<WarehouseMaterialOutput> materialWarehouselInputs);
    }
}