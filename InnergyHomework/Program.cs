﻿using InnergyHomework.Formatters;
using InnergyHomework.Parsers;
using InnergyHomework.Services;

namespace InnergyHomework
{
    class Program
    {
        private const string _dataInputLocation = "../../../../data_input.txt";
        private const string _dataOutputLocation = "data_output.txt";

        static void Main()
        {
            var inputData = new DataReaderService(new FileDataReader(_dataInputLocation)).ReadData();
            var outputDataParsed = new DataFormatService(new HomeworkDataFormatter()).Parse(MaterialWarehouseParser.Parse(inputData));
            new DataStorageService(new FileDataStorage(_dataOutputLocation)).StoreData(outputDataParsed);
        }
    }
}
