﻿using InnergyHomework.Extensions;
using InnergyHomework.Models;
using System.Collections.Generic;

namespace InnergyHomework.Parsers
{
    public class WarehouseMaterialParser
    {
        public IEnumerable<WarehouseMaterialOutput> Parse(IEnumerable<MaterialWarehouselInput> materialWarehouselInputs)
        {
            var result = new List<WarehouseMaterialOutput>();

            foreach (var materialWarehouseInput in materialWarehouselInputs)
            {
                foreach (var materialWarehouseQuantity in materialWarehouseInput.MaterialWarehouseQuantityInputs)
                {
                    if(!result.HasWarehouse(materialWarehouseQuantity.Warehouse))
                    {
                        result.Add(new WarehouseMaterialOutput()
                        {
                            WarehouseName = materialWarehouseQuantity.Warehouse
                        });
                    }

                    result.AppendMaterialToWarehouse(materialWarehouseQuantity.Warehouse,
                        materialWarehouseInput.MaterialId,
                        materialWarehouseQuantity.Quantity);
                }
            }

            return result;
        }
    }
}
