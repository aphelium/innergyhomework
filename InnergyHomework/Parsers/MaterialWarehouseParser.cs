﻿using InnergyHomework.Models;
using System.Collections.Generic;

namespace InnergyHomework.Parsers
{
    public static class MaterialWarehouseParser
    {
        public static IEnumerable<MaterialWarehouselInput> Parse(IEnumerable<string> inputData)
        {
            var result = new List<MaterialWarehouselInput>();

            if (inputData is null)
                return result;

            foreach (var data in inputData)
            {
                if (string.IsNullOrWhiteSpace(data))
                    continue;

                var splits = data.Split(';');
                result.Add(new MaterialWarehouselInput()
                {
                    MaterialName = splits[0].Trim(),
                    MaterialId = splits[1].Trim(),
                    MaterialWarehouseQuantityInputs = MaterialWarehouseQuantityParser.Parse(splits[2])
                }); ;
            }

            return result;
        }
    }
}
