﻿using InnergyHomework.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InnergyHomework.Parsers
{
    public static class MaterialWarehouseQuantityParser
    {
        public static IEnumerable<MaterialWarehouseQuantityInput> Parse(string inputData)
        {
            var result = new List<MaterialWarehouseQuantityInput>();

            if (string.IsNullOrWhiteSpace(inputData))
                return result;

            foreach (var data in inputData.Split('|').ToList())
            {
                if (string.IsNullOrWhiteSpace(data))
                    continue;

                var splits = data.Split(',');
                var quantity = 0;
                try
                {
                    quantity = Convert.ToInt32(splits[1].Trim());
                }
                catch (Exception)
                {
                    //
                }

                result.Add(new MaterialWarehouseQuantityInput()
                {
                    Warehouse = splits[0].Trim(),
                    Quantity = quantity
                });
            }

            return result;
        }
    }
}
